const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//--- Check email if existing---//
	module.exports.checkEmailExists = (request, response, next) =>{

		return User.find({email:request.body.email}).then(result =>{
			console.log(request.body.email);
			let message = ``;

			if(result.length >0){
				message = `The ${request.body.email} is already taken, please use other email.`
				return response.send(message);
			}
			else{
				next()
			}
		})
	}

// --- User Registration ---//
	module.exports.registerUser = (request, response) =>{
		let newUser = new User({
			email: request.body.email,
			password: bcrypt.hashSync(request.body.password, 10),
		})
		return newUser.save().then(user => {
			console.log(user);
			response.send(`Congratulations! Your email ${newUser.email} is successfully registered.`)
		}).catch(error =>{
			console.log(error);
			response.send(`There was an error during your registration. Please try again.`)
		})
	}


//--- User Authentication ---//
	module.exports.loginUser = (request, response) =>{

		return User.findOne({email : request.body.email}).then(result =>{
			console.log(result);

			if(result === null){
				response.send(`The email provided is not yet registered. Register first.`);
			}
			else{
				const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

				if(isPasswordCorrect){
					let token = auth.createAccessToken(result);
					console.log(token);
					return response.send({accessToken: token});
				}
				else{
					return response.send(`Incorrect password. Please try again!`);
				}
			}
		})
	}


//--- Set User as Admin (Admin only) ---//
	module.exports.updateRole = (request, response) =>{
		let token = request.headers.authorization;
		let userData = auth.decode(token);
		let idToBeUpdated = request.params.userId;

		if(userData.isAdmin){
			return User.findByIdAndUpdate(idToBeUpdated).then(result => {
				let update = {
					isAdmin : !result.isAdmin
				};
				return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document =>{
					document.password = "Confidential";
					return response.send(document)}).catch(err =>response.send(err))
			}).catch(err => response.send(err))
		}
		else{
			return response.send("You don't have access on this page")
		}
	}


// --- Retrieve User's details --- //
	module.exports.getProfile = (request, response) =>{

		return User.findById(request.body.id).then(result => {
			result.password = 'Confidential';
			console.log(result);
			return response.send(result);
		}).catch(error => {
			console.log(error);
			return response.send(error);
		})
	}


// --- Add to cart --- //
	module.exports.addToCart = async (request, response) => {
		const token = request.headers.authorization;
		let userData = auth.decode(token);

		if(!userData.isAdmin){
			let data = {
				userId: userData.id,
				productId: request.body.productId,
				quantity: request.body.quantity
			}
			
			let productInfo = await Product.findById(data.productId).then(result => {
				let productDetails = {
					productName : result.productName,
					price : result.price,
				}
				let isUserUpdated = User.findById(data.userId).then(result => {
					result.cart.push({
						products : {
							productId: data.productId,
							productName: productDetails.productName,
							price: productDetails.price
						},
							quantity : data.quantity,
							totalAmount : productDetails.price * data.quantity
					})

					return result.save().then(success => {
						return true;
					}).catch(err => {return false;})
				}).catch(err => {return response.send(false)});

				isUserUpdated ? response.send("Product added to cart!") : response.send("We encountered an error in your purchase, please try again!")

			}).catch(err => {return response.send(false)});
		}
		else{
			return response.send("An admin cannot add an item to cart. Please use a buyer account");
		}
		}


// --- Orders --- //

	module.exports.checkout = async (request, response) => {
		const token = request.headers.authorization;
		let userData = auth.decode(token);

		if(!userData.isAdmin){
			let data = {
				productId: request.body.productId,
				userId: userData.id,
				quantity: request.body.quantity,
				discount: 0
			}
				let isProductUpdated = await Product.findById(data.productId).then(result => {
					result.orders.push({
						userId: data.userId
					})

					result.stocks -= data.quantity;

					return result.save().then(success => {
						return true;
					}).catch(err => {return false;})
				}).catch(err => {return response.send(false)});

				let productInfo = await Product.findById(data.productId).then(result => {
					let productDetails = {
						productName : result.productName,
						price : result.price,	
					}

					let isUserUpdated = User.findById(data.userId).then(result => {

						let totalAmount = productDetails.price*data.quantity;
						if (totalAmount >= 20000){
							data.discount = 1000
						}
						let grandTotal = totalAmount - data.discount;

						result.orders.push({
							products : {
								productName: productDetails.productName,
								price: productDetails.price,
								quantity : data.quantity
							},
							totalAmount : totalAmount,
							discount : data.discount,
							grandTotal :grandTotal
						})

						return result.save().then(success => {
							return true;
						}).catch(err => {return false;})
					}).catch(err => {return response.send(false)});
				}).catch(err => {return response.send(false)});

				(isProductUpdated) ? response.send("Thank you for your purchase!") : response.send("We encountered an error in your purchase, please try again!")
		}
		else{
			return response.send("An admin cannot checkout an item. Please use a buyer account");
		}
	}


